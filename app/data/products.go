package data

import (
	"context"
	"fmt"
	"log"
)

// ErrProductNotFound is an error raised when a product can not be found in the database
var ErrProductNotFound = fmt.Errorf("Product not found")

// Product defines the structure for an API product
type App struct {
	// the id for the product
	//
	// required: false
	// min: 1
	ID int `json:"id"` // Unique identifier for the product

	// the name for this poduct
	//
	// required: true
	// max length: 255
	Name string `json:"name" validate:"required"`

	// the description for this poduct
	//
	// required: false
	// max length: 10000
	Description string `json:"description"`
	// the Domain for the product
	//
	// required: true
	// pattern: [a-z]+-[a-z]+-[a-z]+
	Domain string `json:"domain" validate:"domain"`
	// the Domain for the product
	//
	// required: true
	// pattern: [a-z]+-[a-z]+-[a-z]+
	ClientToken string `json:"client_token" validate:"client_token"`
}

// AddApp adds a new product to the database
func (r *PostgresRepository) AddApp(app App) error {
	// get the next id in sequence
	_, err := r.db.Exec("INSERT INTO app(id, name, description,domain) VALUES($1, $2, $3)", app.ID, app.Description, app.Domain)
	return err
}

// GetProducts returns all products from the database
func (r *PostgresRepository) GetApps(skip uint64, take uint64) ([]App, error) {
	rows, err := r.db.Query("SELECT * FROM app ORDER BY id DESC OFFSET $1 LIMIT $2", skip, take)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = rows.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	//pars all rows to arry of apps

	var apps []App
	for rows.Next() {
		app := App{}
		if err = rows.Scan(&app.ID, &app.Name, &app.Description, &app.Domain); err == nil {
			apps = append(apps, app)
		}

	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return apps, nil
}