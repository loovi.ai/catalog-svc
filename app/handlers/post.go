package handlers

import (
	"net/http"

	data "gitlab.com/loovi.ai/catalog-svc/data"
)

// swagger:route POST /products products createProduct
// Create a new product
//
// responses:
//	200: productResponse
//  422: errorValidation
//  501: errorResponse

// Create handles POST requests to add new products
func (p *data.App) Create(rw http.ResponseWriter, r *http.Request) {
	// fetch the product from the context
	prod := r.Context().Value(KeyProduct{}).(data.App)

	p.l.Printf("[DEBUG] Inserting product: %#v\n", prod)
	data.AddApp(prod)
}
