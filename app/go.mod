module gitlab.com/loovi.ai/catalog-svc

go 1.14

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.6.0
	github.com/nicholasjackson/env v0.6.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
